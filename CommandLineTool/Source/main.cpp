//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>

void factorialFunction (int counterInput);

int main()
{
    
    int intNum;
    
    std::cin >> intNum;
    
    factorialFunction(intNum);
    
    
    
    return 0;
}

void factorialFunction (int counterInput)
{
    int temp = counterInput;
    
    for (int i = 1; i < temp; i++)
    {
        counterInput = counterInput * i;
        
    }
    
    std::cout << "Factorial = " << counterInput << "\n";
}
